package com.example.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.model.Person;

@Profile("dev")
@Repository
public class JdbcPersonDao implements PersonDao {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Person findById(Long id) {
		return jdbcTemplate.queryForObject("select id, first_name, last_name from t_person where id = ?",
				new PersonRowMapper(), id);
	}

	@Override
	public void create(Person person) {
		jdbcTemplate.update("insert into t_person(id,first_name,last_name) values(t_person_sequence.nextval,?,?)",
				person.getFirstName(), person.getLastName());
	}

	@Override
	public void update(Person person) {
		jdbcTemplate.update("update t_person set first_name = ?, last_name = ? where id = ?", person.getFirstName(),
				person.getLastName(), person.getId());
	}

	@Override
	public void delete(Long id) {
		jdbcTemplate.update("delete from t_person where id = ?", id);
	}

	@Override
	public Person findByName(String name) {
		return jdbcTemplate.queryForObject("select id, first_name, last_name from t_person where last_name = ?",
				new PersonRowMapper(), name);
	}

	@Override
	public String aspectTest(String input) {
		throw new UnsupportedOperationException("Not implemented yet.");
	}
	
	private class PersonRowMapper implements RowMapper<Person> {

		@Override
		public Person mapRow(ResultSet rs, int i) throws SQLException {
			Person person = new Person();
			person.setId(rs.getLong("id"));
			person.setFirstName(rs.getString("first_name"));
			person.setLastName(rs.getString("last_name"));
			return person;
		}
	}
}
