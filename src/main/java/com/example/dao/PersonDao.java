package com.example.dao;

import com.example.model.Person;

public interface PersonDao {
	Person findById(Long id);
	void create(Person person);
	void update(Person person);
	void delete(Long id);
	Person findByName(String name);
	String aspectTest(String input);
}
