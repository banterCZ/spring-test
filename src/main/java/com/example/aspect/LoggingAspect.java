package com.example.aspect;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;


@Aspect
public class LoggingAspect {
	
	@Around("within(@org.springframework.stereotype.Repository *)")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

		try {
			System.out.println("logAround() is running!");
			System.out.println("hijacked method : " + joinPoint.getSignature().getName());
			System.out.println("hijacked arguments : " + Arrays.toString(joinPoint.getArgs()));

			System.out.println("Around before is running!");
			return joinPoint.proceed();
		} finally {
			System.out.println("Around after is running!");
			System.out.println("******");	
		}
	}
	
}