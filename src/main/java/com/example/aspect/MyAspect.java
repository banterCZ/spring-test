package com.example.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by Lubos Racansky
 */
@Aspect
@Component
public class MyAspect {

    @Around("execution(* com.example.dao.PersonDao.aspectTest(..))")
    public String fakeReturn() {
        return "test";
    }
}
